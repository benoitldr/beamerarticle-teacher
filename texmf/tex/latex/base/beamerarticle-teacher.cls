\ProvidesClass{beamerarticle-teacher}[2018/11/25 V4 beamerarticle-teacher]
\newif\ifisteach
\newif\ifisbm
\newif\ifisdys
\newif\ifisatrois

\DeclareOption{beamer}{\isbmtrue\isteachtrue}
\DeclareOption{teacher}{\isbmfalse\isteachtrue}
\DeclareOption{student}{\isbmfalse\isteachfalse}
\DeclareOption{dyst}{\isbmfalse\isdystrue\isteachfalse}
\DeclareOption{a3}{\isatroistrue}

\newcommand{\atrois}[2]
{
\ifisatrois
#1
\else
#2
\fi
}

\newcommand{\teacher}[1]{
  \ifisteach
  #1
  \fi
}

\newcommand{\student}[1]{
  \ifisteach
  \else
  #1
  \fi
}

\newcommand{\dys}[2]{
  \ifisdys
  #1
  \else
  #2
  \fi
}

\ProcessOptions\relax
\ifisbm
{
  \ExecuteOptions{beamer}
}
\else
{
  \ifisteach
  {
  \ExecuteOptions{teacher}
  }
  \else
  {
  \ExecuteOptions{student}
  }
  \fi
}
\fi

\usepackage{etoolbox}
\newbool{student}
\newbool{teacher}

\ifisteach
\setbool{student}{false}
\setbool{teacher}{true}
\else
\setbool{student}{true}
\setbool{teacher}{false}
\fi

%https://tex.stackexchange.com/questions/73631/boolean-based-environments
\newenvironment{teach}{%
\ifbool{teacher}
{
%\color{black!70}
\let\teacher@i\relax\let\endteacher@i\relax
}
{
\def\teacher@i{\setbox\z@\vbox\bgroup}%
  \def\endteacher@i{\egroup}
}
\teacher@i%
}  
{%
  \endteacher@i%
}
\newenvironment{stud}{%
  \ifbool{student}
  {
    \let\student@i\relax\let\endstudent@i\relax
  }
  {
    \def\student@i{\setbox\z@\vbox\bgroup}%
      \def\endstudent@i{\egroup}
  }
  \student@i%
}  
{%
  \endstudent@i%
}

%\usepackage{fancybox}

\ifisbm
%BEAMER
%https://tex.stackexchange.com/questions/418935/pass-an-option-from-a-class-to-another
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{beamer}}

\ProcessOptions\relax

\LoadClass[xcolor=dvipsnames,aspectratio=1610,french]{beamer}
%\LoadClass[xcolor=dvipsnames,french]{beamer}
\mode<presentation>
\usecolortheme{rose}
\useoutertheme{infolines}
\useoutertheme{tree}
\beamertemplatenavigationsymbolsempty
%gets rid of bottom navigation bars
%\setbeamertemplate{footline}[page number]{}
%\setbeamertemplate{footline}[frame number]{}
%gets rid of navigation symbols
%\setbeamertemplate{navigation symbols}{}
%\setbeamertemplate{navigation symbols}{}
% Default enumerate
%\setbeamertemplate{enumerate items}[default]
% Correct number for theorems
% \setbeamertemplate{theorems}[numbered]
% Display math as in article
\usefonttheme[onlymath]{serif}
% Change itemize
\setbeamertemplate{enumerate subitem}{\alph{enumii}.}
%Numerotation section
%\defbeamertemplate{section in toc}{sections numbered roman}{%
%    \leavevmode%
%        \MakeUppercase{\romannumeral\inserttocsectionnumber}.\ %
%        \inserttocsection\par}
%        \setbeamertemplate{section in toc}[sections numbered roman]
%        \AtBeginSection[]
%{
%    \begin{frame}
%    \frametitle{Table des mati\`eres}
%    \tableofcontents[currentsection,currentsubsection,hideothersubsections]
%        \end{frame}
%}
%\AtBeginSubsection[]
%{
%    \begin{frame}
%    \frametitle{Table des mati\`eres}
%    \tableofcontents[currentsection,currentsubsection,
%        hideothersubsections,hideothersections,sectionstyle=show/shaded,subsectionstyle=show/shaded]
%            \end{frame}
%}
\else
%REPORT
% Load class
\newcommand\myfontsize@fontsize{Default size}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{extarticle}}
\DeclareOption{10pt}{\renewcommand\myfontsize@fontsize{10pt}}
\DeclareOption{11pt}{\renewcommand\myfontsize@fontsize{11pt}}
\DeclareOption{12pt}{\renewcommand\myfontsize@fontsize{12pt}}
\DeclareOption{14pt}{\renewcommand\myfontsize@fontsize{14pt}}
\DeclareOption{17pt}{\renewcommand\myfontsize@fontsize{17pt}}
\ProcessOptions\relax
%\dys{
%	%TODO 16pt (opendyslexic)
%		\LoadClass[a4paper,17pt,french]{extbook}
%	\renewcommand{\baselinestretch}{1.5}
%	\atrois{
%		\RequirePackage[a3paper,top=2cm, bottom=2cm, 
%			right=1cm,left=1cm]{geometry}
%		\setlength\columnsep{30pt}
%		\setlength{\columnseprule}{1pt}
%	}
%	{
%		\RequirePackage[a4paper, top=1.5cm, bottom=1.5cm, 
%			right=1.5cm,left=1.5cm]{geometry}
%	}
%}
%{
	\teacher{
		\LoadClass[a4paper,\myfontsize@fontsize,french,twoside]{extbook}
		\RequirePackage[hmarginratio=1:1,a4paper, top=2cm, bottom=2cm, 
			right=1cm,left=1cm]{geometry}
	}
	\student{
		\LoadClass[a4paper,\myfontsize@fontsize,french,twoside]{book}
		\RequirePackage[hmarginratio=1:1,a4paper, top=2cm, bottom=2cm, 
			right=1cm,left=1cm]{geometry}
	}
	%TODO package clash car charger deux fois (voir solution sur chatpgt)
	%\atrois{
	%	\RequirePackage[a3paper,landscape,top=2cm, bottom=2cm, 
	%		right=1cm,left=1cm]{geometry}
	%	\setlength\columnsep{30pt}
	%	\setlength{\columnseprule}{1pt}
	%}
	%{
	%	\RequirePackage[a4paper, top=2cm, bottom=2cm, 
	%		right=1.5cm,left=1.5cm]{geometry}
	%}
%}
\RequirePackage{beamerarticle}
\RequirePackage{fancyhdr}
\RequirePackage{lastpage}
%\renewcommand\headrulewidth{1pt}
%\renewcommand\footrulewidth{1pt}
\pagestyle{fancy}
% minitoc (table of contents deep)
%\RequirePackage{minitoc}
%\setcounter{minitocdepth}{3}
%\dys{
%\DeclareSymbolFont{cmletters}{OML}{cmm}{m}{it}
%\DeclareMathSymbol{x}{\mathalpha}{cmletters}{`x}
%\usepackage{enumitem}
%\setitemize{itemsep=200pt}
%\usepackage{cmbright}
%}
%{
%\usepackage{lmodern}
%}
\fi
\usepackage{comment}
\usepackage{currfile}
\ifbool{teacher}
{
    \newenvironment{corr}{}{}
    \excludecomment{elev}{}{}
}
{
  \newenvironment{elev}{}{}
  \excludecomment{corr}
}
\mode<article>{
%\fancyfoot[L]{\scriptsize \em Beno\^it LANDRIEU}
\fancyhead[L]{\scriptsize \em Lyc\'ee Vaucanson 2024-2025}
\fancyfoot[C]{\textbf{\thepage}/\textbf{\pageref{LastPage}}} 
\fancyhead[R]{\small \currfilename}
}
