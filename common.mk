LATEX=xelatex
LATEXOPT=--shell-escape -synctex=1 --enable-write18 --interaction=nonstopmode

STUDENTOPT="\PassOptionsToClass{student}{beamerarticle-teacher}\input{%S}"
TEACHEROPT="\PassOptionsToClass{teacher}{beamerarticle-teacher}\input{%S}"
BEAMEROPT="\PassOptionsToClass{beamer}{beamerarticle-teacher}\input{%S}"
A3OPT="\PassOptionsToClass{a3}{beamerarticle-teacher}\input{%S}"
DYSOPT="\PassOptionsToClass{dys}{beamerarticle-teacher}\input{%S}"

STUDENTSUFF=ELEV
TEACHERSUFF=CORR
BEAMERSUFF=PRES
DYSSUFF=DYS
A3SUFF=A3

LATEXMK=latexmk -f -pdf -silent --outdir=build
LATEXMKTS=latexmk -pdf -silent  --outdir=build
LATEXMKC=latexmk -pvc

USBDIR=/run/media/benoit/BENOITL/00-ENCOURS
USBPRINTDIR=/run/media/benoit/BENOITL/00-TOPRINT
CLOUDDIR=/Users/benoit/ownCloud/COURS
ROOM=$(GITLAB)/spip/TSTI2D

tsstud: $(TEXSRC).tex
	$(LATEXMKTS) -pdflatex='$(LATEX) $(LATEXOPT) %O $(STUDENTOPT)' $(tsstud)

tsteach: $(TEXSRC).tex
	$(LATEXMKTS) -pdflatex='$(LATEX) $(LATEXOPT) %O $(TEACHEROPT)' $(tsteach)

tsbeamer: $(TEXSRC).tex
	$(LATEXMKTS) -pdflatex='$(LATEX) $(LATEXOPT) %O $(BEAMEROPT)' $(tsbeamer)

stud: $(TEXSRC).tex
	$(LATEXMK) -pdflatex='$(LATEX) $(LATEXOPT) %O $(STUDENTOPT)' -jobname='$(TEXSRC)-$(STUDENTSUFF)' $(stud)

teach: $(TEXSRC).tex
	$(LATEXMK) -pdflatex='$(LATEX) $(LATEXOPT) %O $(TEACHEROPT)' -jobname='$(TEXSRC)-$(TEACHERSUFF)' $(teach)

beamer: $(TEXSRC).tex
	$(LATEXMK) -pdflatex='$(LATEX) $(LATEXOPT) %O $(BEAMEROPT)' -jobname='$(TEXSRC)-$(BEAMERSUFF)' $(beamer)

a3: $(TEXSRC).tex
	$(LATEXMK) -pdflatex='$(LATEX) $(LATEXOPT) %O $(A3OPT)' -jobname='$(TEXSRC)-$(A3SUFF)' $(a3)

article: $(TEXSRC).tex
	make stud
	make teach

all: $(TEXSRC).tex
	make stud
	make teach
	make beamer

studpdf: $(TEXSRC).tex
	make stud
	cp build/$(TEXSRC)-$(STUDENTSUFF).pdf $(ROOM)


usb-student:
	make a3
	cp build/$(TEXSRC)-$(A3SUFF).pdf $(USBDIR)
	ps2pdf $(USBDIR)/$(TEXSRC)-$(A3SUFF).pdf $(USBPRINTDIR)/$(TEXSRC)-$(A3SUFF).pdf 
usb-student:
	make stud
	cp build/$(TEXSRC)-$(STUDENTSUFF).pdf $(USBDIR)
	ps2pdf $(USBDIR)/$(TEXSRC)-$(STUDENTSUFF).pdf $(USBPRINTDIR)/$(TEXSRC)-$(STUDENTSUFF).pdf 
usb-teacher:
	cp build/$(TEXSRC)-$(TEACHERSUFF).pdf $(USBDIR)

usb-beamer:
	cp build/$(TEXSRC)-$(BEAMERSUFF).pdf $(USBDIR)

usb-all:
	make usb-student
	make usb-teacher
	make usb-beamer

cleanbuild:
	cd build && rm -rf *.aux *.bbl *.bcf *.blg *.fdb_latexmk *.fls *.log *.maf *.mtc* *.out *.run.xml *.synctex.gz *.nav *.toc *.snm *.gnuplot *.thm *.vrb *.table *.dvi *.ps
clean:
	rm -rf *.aux *.bbl *.bcf *.blg *.fdb_latexmk *.fls *.log *.maf *.mtc* *.out *.run.xml *.synctex.gz *.nav *.toc *.snm *.gnuplot *.thm *.vrb *.table *.dvi *.ps

cleanall:
	make clean
	rm -rf *.pdf
cleanallbuild:
	make cleanbuild
	cd build && rm -rf *.pdf
